<?php

/**
 *  [0] Basics
 *      PHPUnit 11.3.1 
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LeptonBasicsTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonBasicsTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonBasicsTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonBasicsTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use LEPTON_basics;

//  [4] Here we go
class LeptonBasicsTest extends TestCase
{
    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2.0]
        require_once dirname(__DIR__) . "/upload/config/config.php";
    }

    #[DataProvider('getTestGetDaysData')]
    public function testGetDays(string|null $lang, bool $abbr, array $expected)
    {
        if ($lang == null)
        {
            $actual = LEPTON_basics::get_days();
        } else {
            $actual = LEPTON_basics::get_days($lang, $abbr);        
        }
        $this->assertEquals($expected, $actual);
    }
    
    static public function getTestGetDaysData(): array
    {
        return [
            'no param'  => [
                'lang'  => NULL,
                'abbr'  => false,
                'expected' => [1 => "Monday", 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
            ],
            'da_DK'  => [
                'lang'  => 'da_DK',
                'abbr'  => false,
                'expected' => [1 => "mandag", 'tirsdag', 'onsdag', 'torsdag', 'fredag', 'lørdag', 'søndag']
            ],
            'portugal abbr.'  => [
                'lang'  => 'pt_PT',
                'abbr'  => true,
                'expected' => [1 => "Seg", 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom']
            ]
        ];
    }
    
    #[DataProvider('getTestGetMonthsData')]
    public function testGetMonths(string|null $lang, bool $abbr, array $expected)
    {
        if ($lang == null)
        {
            $actual = LEPTON_basics::get_months();
        } else {
            $actual = LEPTON_basics::get_months($lang, $abbr);        
        }
        $this->assertEquals($expected, $actual);
    }
    
    static public function getTestGetMonthsData(): array
    {
        return [
            'no param'  => [
                'lang'  => NULL,
                'abbr'  => false,
                'expected' => [
                    1 => 'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ]
            ],
            'portugal abbr.'  => [
                'lang'  => 'pt_PT',
                'abbr'  => true,
                'expected' => [
                    1 => 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'
                ]
            ]
        ];
    }
}

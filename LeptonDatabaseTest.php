<?php

/**
 *  [0] Basics (configurations and settings)
 *  php.ini:
 *      pdo_mysql.default_socket = /Applications/MAMP/tmp/mysql/mysql.sock
 *
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LeptonDatabaseTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonDatabaseTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonDatabaseTest.php
 *
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use LEPTON_database;
use LEPTON_handle;

class LeptonDatabaseTest extends TestCase
{
    protected bool $keepValuesInDatabase = true;

    public string $tablename = "xtest_lepton_database";
    public LEPTON_database $database;

    public function setUp(): void
    {
        //  [1]
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        require_once dirname(__DIR__) . "/upload/config/config.php";

        //  [2]
        $this->database = LEPTON_database::getInstance();

        //  [3]
        $this->initDatabaseTable();
    }

    public function initDatabaseTable()
    {
        // 1 look for table
        $aAllTables = $this->database->list_tables(TABLE_PREFIX);

        if (!in_array($this->tablename, $aAllTables)) {
            // install table
            $table_fields = "
                `id`             int(11) NOT NULL AUTO_INCREMENT,
                `string`         varchar(128) DEFAULT '',
                `generated_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY ( `id` )
                ";

            LEPTON_handle::install_table($this->tablename, $table_fields);
        }
    }

    public function cleanUpData(int $iLastID)
    {
        if (false === $this->keepValuesInDatabase) {
            $this->database->simple_query(sprintf(
                "DELETE FROM `%s` WHERE `id`=%d",
                TABLE_PREFIX . $this->tablename,
                $iLastID
            ));
        }
    }

    #[DataProvider('dataForCrypt')]
    public function testCrypt(string|int|null $value, string $expected)
    {
        $tablename = TABLE_PREFIX . $this->tablename;

        $this->database->secure_build_and_execute(
            'insert',
            $tablename,
            [
                'string' => $value
            ],
            "",
            [
                'string'
            ]
        );

        $iLastID = $this->database->get_one("SELECT LAST_INSERT_ID() FROM `" . $tablename . "`");

        $actual = $this->database->get_one("SELECT `string` FROM `" . $tablename . "` WHERE `id` = " . $iLastID);

        $this->cleanUpData($iLastID);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Dataprovider for the test above. Keep in mind that the
     * expected values depend on the basic setting of the LEPTON-CMS installation
     * and the values inside the lepton.ini!
     *
     * key      = 'c512de2d86db40bbbdefa139e8f55203'
     * cipher   = 'AES-256-CBC'
     * iv       = 'LuebFNOkX2W%:onZ'
     *
     */
    public static function dataForCrypt(): array
    {
        return [
            'simple string' => [
                'value'    => "Änderungen der Öffnungzeiten in Göttingen.",
                'expected' => "yyt4Slr7eUtvpib6oFB95B86TfeBTCrScpHeE3lJCkrCSi6So8TjzhrlfIU1F9m6"
            ],
            'NULL' => [
                'value'    => null,
                'expected' => ""
            ],
            '0 (null) as integer' => [
                'value'    => 0,
                'expected' => "1GdpxjDxGHRaT3Obl7z5DA=="
            ],
            '0 (null) as string' => [
                'value'    => '0',
                'expected' => "1GdpxjDxGHRaT3Obl7z5DA=="
            ],
            'empty string' => [
                'value'    => "",
                'expected' => "jVa9mU9u5r5T9W7IC+jJGQ=="
            ],

        ];
    }

    #[DataProvider('dataForDecrypt')]
    public function testDecrypt(string|int|null $value, string|null $expected)
    {
        $tablename = TABLE_PREFIX . $this->tablename;

        $this->database->build_and_execute(
            'insert',
            $tablename,
            [
                'string' => $value
            ]
        );

        $iLastID = $this->database->get_one("SELECT LAST_INSERT_ID() FROM `" . $tablename . "`");

        $actual = $this->database->secure_get_one("SELECT `string` FROM `" . $tablename . "` WHERE `id` = " . $iLastID);

        $this->cleanUpData($iLastID);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Dataprovider for the test above. Keep in mind that the
     * expected values depend on the basic setting of the LEPTON-CMS installation
     * and the values inside the lepton.ini!
     *
     * key      = 'c512de2d86db40bbbdefa139e8f55203'
     * cipher   = 'AES-256-CBC'
     * iv       = 'LuebFNOkX2W%:onZ'
     *
     */
    public static function dataForDecrypt(): array
    {
        return [
            'simple string' => [
                'value' => "yyt4Slr7eUtvpib6oFB95B86TfeBTCrScpHeE3lJCkrCSi6So8TjzhrlfIU1F9m6",
                'expected' => "Änderungen der Öffnungzeiten in Göttingen."
            ],
            'NULL' => [
                'value'    => "",
                'expected' => null
            ],
            '0 (null) as string' => [
                'value' => "1GdpxjDxGHRaT3Obl7z5DA==",
                'expected'    => '0'
            ],
            'empty string' => [
                'value' => "jVa9mU9u5r5T9W7IC+jJGQ==",
                'expected'    => ""
            ]
        ];
    }
    
    #[DataProvider('dataForTestSelectAddonTables')]
    public function testSelectAddonTables(string $name, array $expected)
    {
        $actual = $this->database->select_addon_tables($name);
        $this->assertEquals($expected, $actual);
        
    }
    
    public static function dataForTestSelectAddonTables(): array
    {
        return [
            'test records' => [
                'name' => 'mod_records',
                'expected' => ['mod_records', 'mod_records_history', 'mod_records_settings', 'mod_records_trade_0', 'mod_records_trade_1']
            ],
            'test news' => [
                'name'  => 'mod_news',
                'expected' => ['mod_news_comments', 'mod_news_groups', 'mod_news_posts', 'mod_news_settings']
            ],
            'test none' => [
                'name'  => 'mod_gibtEsNicht',
                'expected' => []
            ]
        ];
    }
}

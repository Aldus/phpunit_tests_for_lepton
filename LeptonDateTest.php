<?php

/**
 *  [0] Basics
 *      PHPUnit 11.4.3 
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LeptonDateTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonDateTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonDateTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonDateTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use LEPTON_date;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

//  [4] Here we go
class LeptonDateTest extends TestCase
{
    public const TEST_TIMESTAMP   = 1661585552; // 27. August 2022
    public const TEST_TIMESTAMP_2 = 1737215115; // 18. Januar 2025

    private LEPTON_date $oLEPTON_date;

    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/config/config.php";

        //  [2]
        $this->oLEPTON_date = LEPTON_date::getInstance(true);
    }
    
    #[DataProvider('formatWithMySQLProvider')]
    public function testFormatWithMySQL(
        string $format,
        int $timestamp,
        string $language,
        string $expected
    ) {

        $actual = $this->oLEPTON_date->formatWithMySQL(
            $format,
            $timestamp,
            $language
        );

        $this->assertEquals($expected, $actual);
    }

    public static function formatWithMySQLProvider(): array
    {
        return [
            'en d M Y' => [
                'format'    => "d M Y",
                'timestamp' => self::TEST_TIMESTAMP,
                'language'  => "en",
                'expected'  => "27. August 2022"
            ],
            'fr d M Y' => [
                'format'    => "d M Y",
                'timestamp' => self::TEST_TIMESTAMP,
                'language'  => "fr",
                'expected'  => "27. août 2022"
            ],
            'en l, jS F, Y' => [
                'format'    => "l, jS F, Y",
                'timestamp' => self::TEST_TIMESTAMP,
                'language'  => "en",
                'expected'  => "Saturday, 27th August, 2022"
            ],
            'de own format' => [
                'format'    => "\Am\ %W, \den\ %e. %M %Y\, um\ %k:%i \Uhr\.",
                'timestamp' => self::TEST_TIMESTAMP,
                'language'  => "de",
                'expected'  => "Am Samstag, den 27. August 2022, um 9:32 Uhr."
            ],
            'fr own format' => [
                'format'    => "\Le\ %W %e %M %Y\, nous irons dans un musée à\ %k \h %i.",
                'timestamp' => 1173628880,
                'language'  => "fr",
                'expected'  => "Le dimanche 11 mars 2007, nous irons dans un musée à 17 h 01."
            ],
            'de before 1970' => [
                'format'    => "\Am\ %W, \den\ %e. %M %Y\, um\ %k:%i \Uhr\.",
                'timestamp' => -120268800,
                'language'  => "de",
                'expected'  => "Am Freitag, den 11. März 1966, um 1:00 Uhr."
            ]
        ];
    }
    
    #[DataProvider('getWeekdayNamesProvider')]
    public function testGetWeekdayNames(string $language, bool $abbr, array $expected)
    {
        $actual = $this->oLEPTON_date->getWeekdayNames($language, $abbr);
        $this->assertEquals($expected, $actual);
    }
    
    public static function getWeekdayNamesProvider(): array
    {
        return [
            'Dansk' => [
                'language' => 'da_DK',
                'abbr'     => false,
                'expected' => [
                    1 => 'mandag', 
                    'tirsdag',
                    'onsdag',
                    'torsdag',
                    'fredag',
                    'lørdag',
                    'søndag'
                ]
            ],
            'Dansk abbr' => [
                'language' => 'da_DK',
                'abbr'     => true,
                'expected' => [
                    1 => 'man', 
                    'tir',
                    'ons',
                    'tor',
                    'fre',
                    'lør',
                    'søn'
                ]
            ]
        ];
    }

    #[DataProvider('toHTMLProvider')]
    public function testToHTML(string $language, string $format, string $expected)
    {
        $this->oLEPTON_date->set_core_language($language);

        $this->oLEPTON_date->sINTLFormat = $format;

        $actual = $this->oLEPTON_date->toHTML(self::TEST_TIMESTAMP_2);

        $this->assertEquals($expected, $actual);
    }

    /**
     * For the formats please see:
     * 
     * @link: https://unicode-org.github.io/icu/userguide/format_parse/datetime/#formatting-dates
     */
    public static function toHTMLProvider(): array
    {
        return [
            'simple INTL' => [
                'language'  => "DE",
                'format'    => 'd. MMMM Y',
                'expected'  => "18. Januar 2025"
            ],
            'simple INTL with hour' => [
                'language'  => "DE",
                'format'    => 'EEEE d. MMMM Y - H:m',
                'expected'  => "Samstag 18. Januar 2025 - 16:45"
            ],
            'simple INTL with hour (FR)' => [
                'language'  => "FR",
                'format'    => 'EEEE d. MMMM Y - H:m',
                'expected'  => "samedi 18. janvier 2025 - 16:45"
            ]
        ];
    }
}

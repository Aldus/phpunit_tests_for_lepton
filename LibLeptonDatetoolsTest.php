<?php

/**
 *  [0] Basics
 *  php.ini:
 *  pdo_mysql.default_socket = /Applications/MAMP/tmp/mysql/mysql.sock
 *
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LibLeptonDatetoolsTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibLeptonDatetoolsTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibLeptonDatetoolsTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibLeptonDatetoolsTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class LibLeptonDatetoolsTest extends TestCase
{
    public const TEST_TIMESTAMP = 1661585552;

    private \lib_lepton_datetools $leptonDateTools;

    public function setUp(): void
    {

        //  [1]
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2]
        require_once dirname(__DIR__) . "/upload/config/config.php";

        //  [3]
        require_once dirname(__DIR__) . "/upload/framework/classes/lepton_request.php";

        //  [4]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }

        //  [5]
        if (!defined("DEFAULT_TIMEZONE_STRING")) {
            define('DEFAULT_TIMEZONE_STRING', "Europe/Berlin");
        }

        //  [6]
        $this->leptonDateTools = \lib_lepton::getToolInstance("datetools");
    }

    #[DataProvider('buildLanguageKeyProvider')]
    public function testBuildLanguageKey(string $lang, string $expected)
    {
        $actual = \lib_lepton_datetools::buildLanguageKey($lang);
        $this->assertEquals($expected, $actual);
    }

    public static function buildLanguageKeyProvider(): array
    {
        return [
            'de' => [
                'lang' => "de",
                'expected' => "de_DE"
            ],
            'fr' => [
                'lang' => "fr",
                'expected' => "fr_FR"
            ],
            'xh' => [
                'lang' => "xh",
                'expected' => "xh_XH"
            ]
        ];
    }

    public function testCheckINTLExtensionExists()
    {
        $actual = $this->leptonDateTools->checkINTLExtensionExists();
        $this->assertEquals(true, $actual);
    }

    public function testToHTML()
    {
        $actual = $this->leptonDateTools->toHTML(self::TEST_TIMESTAMP);
        $this->assertEquals("27.8.2022", $actual);
    }

    #[DataProvider('formatWithMySQLProvider')]
    public function testFormatWithMySQL(
        string $format,
        int $timestamp,
        string $language,
        string $expected
    ) {

        $actual = $this->leptonDateTools->formatWithMySQL(
            $format,
            $timestamp,
            $language
        );

        $this->assertEquals($expected, $actual);
    }

    public static function formatWithMySQLProvider(): array
    {
        return [
            'en d M Y' => [
                'format' => "d M Y", 'timestamp' => self::TEST_TIMESTAMP,
                'language' => "en", 'expected' => "27. August 2022"
            ],
            'fr d M Y' => [
                'format' => "d M Y", 'timestamp' => self::TEST_TIMESTAMP,
                'language' => "fr", 'expected' => "27. août 2022"
            ],
            'en l, jS F, Y' => [
                'format' => "l, jS F, Y", 'timestamp' => self::TEST_TIMESTAMP,
                'language' => "en", 'expected' => "Saturday, 27th August, 2022"
            ],
            'de own format' => [
                'format' => "\Am\ %W, \den\ %e. %M %Y\, um\ %k:%i \Uhr\.",
                'timestamp' => self::TEST_TIMESTAMP, 'language' => "de",
                'expected' => "Am Samstag, den 27. August 2022, um 9:32 Uhr."
            ],
            'fr own format' => [
                'format' => "\Le\ %W %e %M %Y\, nous irons dans un musée à\ %k \h %i.",
                'timestamp' => 1173628880, 'language' => "fr",
                'expected' => "Le dimanche 11 mars 2007, nous irons dans un musée à 17 h 01."
            ],
            'de before 1970' => [
                'format' => "\Am\ %W, \den\ %e. %M %Y\, um\ %k:%i \Uhr\.",
                'timestamp' => -120268800, 'language' => "de",
                'expected' => "Am Freitag, den 11. März 1966, um 1:00 Uhr."
            ]
        ];
    }
    
    #[DataProvider('getWeekdayNamesProvider')]
    public function testGetWeekdayNames(string $language, bool $abbr, array $expected)
    {
        $actual = $this->leptonDateTools->getWeekdayNames($language, $abbr);
        $this->assertEquals($expected, $actual);
    }
    
    public static function getWeekdayNamesProvider(): array
    {
        return [
            'Dansk' => [
                'language' => 'da_DK',
                'abbr'     => false,
                'expected' => [
                    1 => 'mandag', 
                    'tirsdag',
                    'onsdag',
                    'torsdag',
                    'fredag',
                    'lørdag',
                    'søndag'
                ]
            ],
            'Dansk abbr' => [
                'language' => 'da_DK',
                'abbr'     => true,
                'expected' => [
                    1 => 'man', 
                    'tir',
                    'ons',
                    'tor',
                    'fre',
                    'lør',
                    'søn'
                ]
            ]
        ];
    }
}

<?php

/**
 *  [0] Basics
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/FunctionRandomStringTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionRandomStringTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionRandomStringTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionRandomStringTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

//  [4] Here we go
class FunctionRandomStringTest extends TestCase
{
    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2]
        if (!defined("LANGUAGE")) {
            define('LANGUAGE', "EN");
        }

        //  [3]
        //require_once dirname(__DIR__) . "/upload/framework/classes/lepton_core.php";

        //  [4]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }
    }

    public static function randomStringProvider(): array
    {
        return [
            'hex lower' => [
                'numchars'  => 16,
                'type'      => 'hex-lower',
                'regExpr'   => '~^[a-f0-9]{16}$~',
                'expected'  => 1
                ],
            'pass' => [
                'numchars'  => 16,
                'type'      => 'pass',
                'regExpr'   => '~^[a-zA-Z0-9\.\-_]{16}$~', // l@&sHC3Vxm7ZEkO1
                'expected'  => 1
                ]
        ];
    }

    #[DataProvider('randomStringProvider')]
    public function testRandomString(int $numchars, string $type, string $regExpr, int $expected): void
    {
        \LEPTON_handle::register("random_string");

        $randomString = random_string($numchars, $type);
        echo \LEPTON_tools::display($randomString) . "\n";
        $actual = preg_match($regExpr, $randomString);
        $this->assertEquals($expected, $actual);
    }
}

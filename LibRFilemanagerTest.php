<?php

/**
 *  [0] Basics
 *  php.ini:
 *  pdo_mysql.default_socket = /Applications/MAMP/tmp/mysql/mysql.sock
 *
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LibRFilemanagerTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibRFilemanagerTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibRFilemanagerTest.php
 *
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibRFilemanagerTest.php
 *
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class LibRFilemanagerTest extends TestCase
{
    private \lib_r_filemanager $oFilemanager;

    public function setUp(): void
    {

        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        require_once dirname(__DIR__) . "/upload/config/config.php";

        require_once dirname(__DIR__) . "/upload/modules/lib_r_filemanager/classes/lib_r_filemanager.php";

        $this->oFilemanager = \lib_r_filemanager::getInstance();
    }

    /**
     *
     */
    public function testGetSystemLanguage()
    {
        $actual = \lib_r_filemanager::getSystemLanguage();
        $this->assertEquals("en_EN", $actual);
    }

    #[DataProvider('getTestConstantsData')]
    public function testConstants(string|int|array $expected)
    {
        $actual = \lib_r_filemanager::allowed_image_types;
        $this->assertEquals($expected, $actual);
    }

    public static function getTestConstantsData(): array
    {
        return [
            'allowed_image_types' => [
                    'expected' => ['jpg', 'jpeg', 'png', 'gif', 'webp', 'bmp', 'svg', 'ico', 'jxl', 'avif']
                ]
        ];
    }
}

<?php

/**
 *  [0] Basics
 *      PHPUnit 11.3.1 
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/ModDropletsTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/ModDropletsTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/ModDropletsTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/ModDropletsTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

//  [4] Here we go
class ModDropletsTest extends TestCase
{
    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2.0]
        require_once dirname(__DIR__) . "/upload/config/config.php";
        
        //  [2.1]
        require_once dirname(__DIR__) . "/upload/modules/droplets/droplets.php";
        if (!defined("DEBUG"))
        {
            define("DEBUG", false);
        }
    }

    #[DataProvider('GetTestEvalDropletsData')]
    public function testEvalDroplets(string|null $source, string|null $expected): void
    {
        evalDroplets($source);
        
        $this->assertEquals($expected, $source);
    }
    
    // Dataprovider for the test above
    public static function GetTestEvalDropletsData(): array
    {
        return [
            'lorem' => [
                'source'    => "[[lorem]]",
                'expected'  => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Ut odio. Nam sed est. Nam a risus et est iaculis adipiscing. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer ut justo. In tincidunt viverra nisl. Donec dictum malesuada magna. Curabitur id nibh auctor tellus adipiscing pharetra. Fusce vel justo non orci semper feugiat. Cras eu leo at purus ultrices tristique.<br /><br />"
            ],
            'test NULL' => [
                'source'    => NULL,
                'expected'  => NULL
            ],
            'droplet does not exists' => [
                'source'    => "Aladin [[gibtEsNicht?s=123]]",
                'expected'  => "Aladin **No such droplet: gibtEsNicht?s=123**"
            ]
        ];
    }
}

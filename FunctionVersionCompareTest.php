<?php

/**
 *  [0] Basics
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/FunctionVersionCompareTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionVersionCompareTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionVersionCompareTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionVersionCompareTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

//  [4] Here we go
class FunctionVersionCompareTest extends TestCase
{
    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2]
        if (!defined("LANGUAGE")) {
            define('LANGUAGE', "EN");
        }

        //  [3]
        //require_once dirname(__DIR__) . "/upload/framework/classes/lepton_core.php";

        //  [4]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }
    }

    #[DataProvider('versionCompareProvider')]
    public function testVersionCompare(
        string $versionA,
        string $versionB,
        string $compare,
        bool $expected
    ): void
    {
        \LEPTON_handle::register("versionCompare");

        $actual = versionCompare($versionA, $versionB, $compare);
        
        $this->assertEquals($expected, $actual);
    }
    
    public static function versionCompareProvider(): array
    {
        return [
            'simple version A > B' => [
                    'versionA'  => '1.1.0',
                    'versionB'  => '1.2.0',
                    'compare'   => '<=',
                    'expected'  => true
                ],
            'B is beta' => [
                    'versionA'  => '1.1.0',
                    'versionB'  => '1.1.0 beta',
                    'compare'   => '<',
                    'expected'  => true
                ],
            'A is RC2' => [
                    'versionA'  => '1.1.0 RC2',
                    'versionB'  => '1.1.0 RC',
                    'compare'   => '>',
                    'expected'  => true
                ],
            'A and B equal' => [
                    'versionA'  => '1.1.0 RC2',
                    'versionB'  => '1.1.0 RC2',
                    'compare'   => '>=',
                    'expected'  => true
                ]
        ];
    }
}

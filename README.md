# PHPUnit Tests for [LEPTON-CMS][1]


## Preface
This is just a private storage for [PHPUnit][2] test-files for [LEPTON-CMS][1].

## Licences
cc 3.0 sa-by

## Get PHPUnit
- Please read:
https://phpunit.de/announcements/phpunit-11.html

- Get it via wget:  
```text
wget -O phpunit https://phar.phpunit.de/phpunit-11.phar            
```
or
```text
wget https://phar.phpunit.de/phpunit-11.phar  
```  

## Requirement
- PHP >= 8.2 (8.3 recomented)  
- LEPTON-CMS 7.2 (for some new core [class-]methods.  
- PHPUnit 11

## Installation
- 1) A good place is inside a folder "tests" under the "root" of the (local) LEPTON-CMS installation  
under MAMP, or XAMPP, or Laragoon, or something similar.
- 2) Install [PHPUnit][3] inside same (root) level (e.g. by downloding the PHAR file)  
_example_
![folder structure](/img/folders.png)  
- 3) Update ".gitignore" to exclude "tests/" and "phpunit.phar" and ".phpunit.result.cache" from your working directory.  
```html
# git ignore this local files
# LEPTON-CMS VII - MAMP 

.gitignore
.DS_Store
*/.DS_Store
.idea
phpunit.phar
.phpstorm.meta.php
.phpunit.result.cache
tests/
```

### Usage examples
##### point to the current working directory, e.g. 
```text
% cd /Applications/MAMP/htdocs/projekte/LEPTON_VII 
```

##### simple direct call
```text
% php phpunit.phar tests/lepton_requestTest.php
```

##### call by terminal and loged the result inside "aldus.txt"
```text
% php phpunit.phar --log-events-verbose-text aldus.txt  tests/lepton_requestTest.php
```

##### call colors
```text
% php phpunit.phar --colors='always'  tests/lepton_requestTest.php 
```

##### run all Tests
```text
% php phpunit.phar tests 
```

##### run inside the test directory
_keep in mind that you will also have to add '../' to the target folders for phpunit.phar._
```text
% php ../phpunit.phar --colors='always'  ../tests/lepton_requestTest.php
% php ../phpunit.phar --colors='always'  ../tests/lepton_coreTest.php
```

![console output](/img/console.png)  

![laragon terminal](/img/screen_laragon_terminal.png)  

#### additions:
Tests e.g. PHP-Code-sniffer
```text
% phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonSessionTest.php
```

```text
% phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonSessionTest.php
```

#### trouble shooting
- using a specific php version with MAMP try first, or edit \'\.bash_profile\'
```text
% export PATH=/Applications/MAMP/bin/php/php8.4.1/bin:$PATH
```
or
```
\./\.bash\_profile
```


2025.1 - Aldus

[1]: https://lepton-cms.org "LEPTON CMS"
[2]: https://phpunit.de/ "PHPUnit"
[3]: https://phpunit.de/announcements/phpunit-10.html "PHPUnit 10"

<?php

/**
 *  [0] Basics
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/FunctionGetModulVersionTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionGetModulVersionTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionGetModulVersionTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionGetModulVersionTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

//  [4] Here we go
class FunctionGetModulVersionTest extends TestCase
{
    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2]
        if (!defined("LANGUAGE")) {
            define('LANGUAGE', "EN");
        }

        //  [3]
        //require_once dirname(__DIR__) . "/upload/framework/classes/lepton_core.php";

        //  [4]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }
    }

    #[DataProvider('getModuleVersionProvider')]
    public function testGetModulVersion(string $moduleName, bool $useSource, string|null $expected): void
    {
        \LEPTON_handle::register("get_modul_version");

        $actual = get_modul_version($moduleName, $useSource);
        
        $this->assertEquals($expected, $actual);
    }
    
    public static function getModuleVersionProvider(): array
    {
        return [
            'edit area from db' => [
                    'moduleName'  => 'edit_area',
                    'useSource'  => false,
                    'expected'  => '2.3.2'
                ],
            'edit area from source' => [
                    'moduleName'  => 'edit_area',
                    'useSource'  => true,
                    'expected'  => '2.3.2'
                ]
        ];
    }
}

<?php

/**
 *  [0] Basics
 *
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always'  tests/LeptonRequestTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonRequestTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonRequestTest.php
 *
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use LEPTON_request;

//  [4] Here we go
class LeptonRequestTest extends TestCase
{
    public \LEPTON_request|null $oLEPTON_REQUEST = null;

    public function setUp(): void
    {
        //  [1.1]
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [1.2]
        require_once dirname(__DIR__) . "/upload/framework/classes/lepton_request.php";

        //  [1.3]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }

        //  [1.4]
        if (!defined("LANGUAGE")) {
            define('LANGUAGE', "EN");
        }

        //  [2.1]
        $this->oLEPTON_REQUEST = \LEPTON_request::getInstance();
        $this->oLEPTON_REQUEST->error_mode = true;
    }

    #[DataProvider('buildTestPostValues')]
    public function testTestPostValues(array $valuesForPOST, array $testArrayForLEPTON, array $expected)
    {
        foreach ($valuesForPOST as $key => $val) {
            $_POST[$key] = $val;
        }

        $actual = $this->oLEPTON_REQUEST->testPostValues($testArrayForLEPTON);
        $this->assertEquals($expected, $actual);
    }

    /**
     *  This is the data-provider for the test above 'testTestPostValues'.
     */
    public static function buildTestPostValues(): array
    {
        return [
            // --- [1] Simple Values
            '[1] simple values' => [
                    // --- [1.1] These values will be set in the $_POST
                    'valuesForPOST' => [
                            'ein_string' => 'Das ist ein einfacher String',
                            'ein_integer' => 175,
                            'ein_posInt' =>  0,
                            'regExp_ok'  => '1972-01-01T12:22',
                            'regExp_failed'  => 'a1972-01-01T12:22'
                        ],

                    // --- [1.2] This is the test-array inside LEPTON-CMS
                    'testArrayForLEPTON'  => [
                            'ein_string'    => ['type'  => 'string',   'default'   => 'none given!'],
                            'ein_integer'   => ['type'  => 'integer',  'default'   => 0 ],
                            'ein_posInt'    => ['type'  => 'integer+', 'default'   => 1 ],
                            'regExp_ok'    => [
                                'type'  => 'regexp',
                                'default'   => '',
                                'range' => [
                                    'pattern' => '/^[0-9]{4}-[0-9]{2}.*$/'
                                    ]
                                ],
                            'regExp_failed'    => [
                                'type'  => 'regexp',
                                'default'   => '',
                                'range' => [
                                    'pattern' => '/^[0-9]{4}-[0-9]{2}.*$/',
                                    'default'   => 'Fehler!'
                                    ]
                                ]
                        ],

                    // --- [1.3] The expected result of the test in LEPTON-CMS
                    'expected' => [
                            'ein_string' => 'Das ist ein einfacher String',
                            'ein_integer' => 175,
                            'ein_posInt'    => 1,
                            'regExp_ok'     => '1972-01-01T12:22',
                            'regExp_failed' => 'Fehler!'
                        ]
                    ],

            // --- [2] Integers (between 0 and 10)
            '[2] between_0_and_10' => [
                    'valuesForPOST' => [
                            'is_exact_5'              => 5,
                            'is_less_than_0 near'     => -1,
                            'is_greater_than_10 near' => 12
                        ],
                    'testArrayForLEPTON'  => [
                            'is_exact_5'    => [
                                'type'      => 'integer',
                                'default'   => 0,
                                'range'     => ['min' => 0, 'max' => 10]
                            ],
                            'is_less_than_0 near'   => [
                                'type'          => 'integer',
                                'default'       => -1,
                                'range'         => ['min' => 1, 'max' => 10, 'use' => 'near']
                            ],
                            'is_greater_than_10 near' => [
                                'type'      => 'integer',
                                'default'   => 1,
                                'range'     => ['min' => 0, 'max' => 10, 'use' => 'near']
                            ]
                        ],
                    'expected' => [
                            'is_exact_5'              => 5,
                            'is_less_than_0 near'     => 1,
                            'is_greater_than_10 near' => 10
                        ]
                    ],

            // --- [3] Strings - fill up and crop
            '[3] strings_fill_and_cut' => [
                    'valuesForPOST' => [
                            '5_chars_exact'     => 'abcde',
                            '7_chars_fill_up'   => 'abc',
                            '12_chars_crop'     => 'abcdefghijklmnopqrst'
                        ],
                    'testArrayForLEPTON'  => [
                            '5_chars_exact' => [
                                'type'      => 'string',
                                'default'   => '',
                                'range'     => ['min' => 5, 'max' => 5]
                            ],
                            '7_chars_fill_up'   => [
                                'type'      => 'string',
                                'default'   => '',
                                'range'     => ['min' => 7, 'max' => 7, 'use' => 'fill', 'char' => '_']
                            ],
                            '12_chars_crop' => [
                                'type'      => 'string',
                                'default'   => '',
                                'range'     => ['min' => 6, 'max' => 12, 'use' => 'cut']
                            ]
                        ],
                    'expected' => [
                            '5_chars_exact'     => 'abcde',
                            '7_chars_fill_up'   => 'abc_____',
                            '12_chars_crop'     => 'abcdefghijkl'
                        ]
                    ],

            // --- [4] Timestamps and date/time values
            '[4] timestamps tests' => [
                    'valuesForPOST' => [
                            'time_72' => '2023-08-08 01:00:00'
                        ],
                    'testArrayForLEPTON'  => [
                            'time_72'  => [
                                'type'      => 'timestamp',
                                'default'   => '0000-00-00 00:00:00'
                            ]
                        ],
                    'expected' => [
                            'time_72' => '2023-08-08 01:00:00'
                        ]

                ],

             // --- [5] types
            '[5] type tests' => [
                    'valuesForPOST' => [
                            'arrayValues' => [1, 33, 55, 66],
                            'no_array'  => 'Das ist kein array.'
                        ],
                    'testArrayForLEPTON'  => [
                            'arrayValues'  => [
                                'type'      => 'array',
                                'default'   => []
                            ],
                            'no_array'  => [
                                'type'      => 'array',
                                'default'   => []
                            ]
                        ],
                    'expected' => [
                            'arrayValues' => [1,33,55,66],
                            'no_array'  => []
                        ]
                ],

            // --- [6] new date and time keywords/types
            '[6] type tests2' => [
                    'valuesForPOST' => [
                            'time_ok'       => '12:34',
                            'time_failed'   => 'Das ist keine Zeitangabe!',
                            'month_ok'      => '2023-06',
                            'month_failed'  => '2023-06-01 12:31.02',
                            'week_ok'       => '2023-W12',
                            'week_failed'   => '2023-12-02',
                            'time is 0 (int)' => '0'
                        ],
                    'testArrayForLEPTON'  => [
                            'time_ok'   => [
                                'type'      => 'time',
                                'default'   => '00:00'
                            ],
                            'time_failed'  => [
                                'type'      => 'time',
                                'default'   => '00:00'
                            ],
                            'month_ok'  => [
                                'type'      => 'month',
                                'default'   => '1971-01'
                            ],
                            'month_failed'  => [
                                'type'      => 'month',
                                'default'   => '1971-01'
                            ],
                            'week_ok'  => [
                                'type'      => 'week',
                                'default'   => '1971-W01'
                            ],
                            'week_failed'  => [
                                'type'      => 'week',
                                'default'   => '1971-W01'
                            ],
                            'time is 0 (int)' => [
                                'type'      => 'time',
                                'default'   => '00:00'
                            ]
                        ],
                    'expected' => [
                            'time_ok'       => '12:34',
                            'time_failed'   => '00:00',
                            'month_ok'      => '2023-06',
                            'month_failed'  => '1971-01',
                            'week_ok'       => '2023-W12',
                            'week_failed'   => '1971-W01',
                            'time is 0 (int)' => '00:00'
                        ]

                ],
                // --- [7] new core methods
                '[7] core methods' => [
                    'valuesForPOST' => [
                            'aldus_string_ok'     => 'Aladin hat eine Wunderlampe.',
                            'aldus_string_faild'  => 12345,
                            'int as string'       => "12345"
                        ],
                    'testArrayForLEPTON'  => [
                            'aldus_string_ok' => [
                                'type'      => 'string_clean',
                                'default'   => null
                            ],
                            'aldus_string_faild'  => [
                                'type'      => 'string_clean',
                                'default'   => null
                            ],
                            'int as string'  => [
                                'type'      => 'string_clean',
                                'default'   => null
                            ]
                        ],
                    'expected' => [
                            'aldus_string_ok'       => 'Aladin hat eine Wunderlampe.',
                            'aldus_string_faild'   => null,
                            'int as string'      => "12345"
                        ]
                ],
                // --- [8] core method with arrays
                '[8] core methods with arrays' => [
                    'valuesForPOST' => [
                            'only_integers' => [1, 2, 3, 4, 5, 55, "123", "aladin"],
                            'only_strings'  => ["Das", "ist", "ein", "Test", 123435, ["soll null werden"]],
                            'string_secure' => ["sollte", "durch", "gehen", "kein Leerzeichen erlaubt"]
                        ],
                    'testArrayForLEPTON' => [
                            'only_integers' => [
                                'type'      => 'integer+',
                                'default'   => null
                            ],
                            'only_strings'  => [
                                'type'      => 'string_clean',
                                'default'   => null
                            ],
                            'string_secure'  => [
                                'type'      => 'string_secure',
                                'default'   => null
                            ]

                        ],
                    'expected' => [
                            'only_integers' => [1, 2, 3, 4, 5, 55, 123, null],
                            'only_strings'  => ["Das", "ist", "ein", "Test", null, null],
                            'string_secure' => ["sollte", "durch", "gehen", null]
                        ]
                ],
                '[9] boolean' => [
                    'valuesForPOST' => [
                            'simple bool' => [true, false],
                            'one int' => [1, false],
                            'chars and int' => ["1", "true", 0, "false", "geht nicht sinnlos", -1]
                        ],
                    'testArrayForLEPTON' => [
                            'simple bool' => [
                                'type'      => 'bool',
                                'default'   => true
                            ],
                            'one int' => [
                                'type'      => 'bool',
                                'default'   => true
                            ],
                            'chars and int' => [
                                'type'      => 'bool',
                                'default'   => null
                            ]
                        ],
                    'expected' => [
                            'simple bool' => [true, false],
                            'one int'     => [true, false],
                            'chars and int' => [true, true, false, false, null, null]
                        ]
                ],
                '[10]integer evaluate' => [
                    'valuesForPOST' => [
                            'simple calculation' => ["3 * 60 *60","-1 * (10+1)", "'keine zahl'"],
                            'mit range' => ["10*4 + 5", "-1 * 3"]
                        ],
                    'testArrayForLEPTON' => [
                            'simple calculation' => [
                                'type'      => 'integer_eval',
                                'default'   => 0
                            ],
                            'mit range' => [
                                'type'      => 'integer_eval',
                                'default'   => 0,
                                'range'     => ['min' => 1, 'max' => 10, 'use' => 'near']
                            ]
                        ],
                    'expected' => [
                            'simple calculation' => [10800, -11, 0],
                            'mit range' => [10, 1]
                        ]
                ]
        ];
    }

    #[DataProvider('buildFilterValueDataset')]
    public function testFilterValue(
        string|int|array|float|null $testValue,
        string $type,
        string|int|array|float|null $default,
        string|array $range,
        string|int|array|float|null $expected
    ) {
        $actual = $this->oLEPTON_REQUEST->filterValue($testValue, $type, $default, $range);
        $this->assertEquals($expected, $actual);
    }

    /**
     *  This is the data-provider for the test above 'testFilterValue'.
     */
    public static function buildFilterValueDataset(): array
    {
        return [
            // --- [1] Simple Values
            '[1] integer+' => [
                    'testValue' => 12345,
                    'type'      => 'integer+',
                    'default'   => 12,
                    'range'     => "",
                    'expected'  => 12345
                ],
            '[2] integer-' => [
                    'testValue' => -123,
                    'type'      => 'integer-',
                    'default'   => -9999,
                    'range'     => "",
                    'expected'  => -123
                ],
            '[3] no integer' => [
                    'testValue' => "das ist kein int.",
                    'type'      => 'integer-',
                    'default'   => 0,
                    'range'     => "",
                    'expected'  => 0
                ],
            '[4] in range' => [
                    'testValue' => 12,
                    'type'      => 'integer',
                    'default'   => 0,
                    'range'     => ['min' => 10, 'max' => 20],
                    'expected'  => 12
                ],
            '[5] out range' => [
                    'testValue' => 12,
                    'type'      => 'integer',
                    'default'   => 0,
                    'range'     => ['min' => 0, 'max' => 10],
                    'expected'  => 0
                ],
            '[6] out range near max' => [
                        'testValue'  => 22,
                        'type'       => 'integer',
                        'default'    => 0,
                        'range'      => [
                                'min' => 5,
                                'max' => 10,
                                'use' => 'near'
                            ],
                        'expected' => 10
                    ],
            '[7] out range near min' => [
                        'testValue'  => 3,
                        'type'       => 'integer',
                        'default'    => 0,
                        'range'      => [
                                'min' => 5,
                                'max' => 10,
                                'use' => 'near'
                            ],
                        'expected' => 5
                    ],
              '[8] out range use' => [
                        'testValue'   => 3,
                        'type'        => 'integer',
                        'default'     => 0,
                        'range'       => [
                                'min' => 5,
                                'max' => 10,
                                'use' => 'default'
                            ],
                        'expected' => 0
                    ],
              '[9] regExp failed use regex default' => [
                        'testValue'  => "Les pensées sont libres",
                        'type'       => 'regExp',
                        'default'    => null,
                        'range'      => [
                                'pattern' => "~^[a-z][a-z0-9 \.-_]+$~i",
                                'default' => "-"
                            ],
                        'expected' => '-'
                    ],
                '[10] password with additional chars' => [
                        'testValue'  => 'parislondon_0123-#@?µ *!',
                        'type'       => 'password',
                        'default'    => '',
                        'range'      => '',
                        'expected'   => 'parislondon_0123-#@?µ *!'
                    ],
                '[11] string_allowed' => [
                        'testValue'  => '<p>parislondon<p><img>',
                        'type'       => 'string_allowed',
                        'default'    => '',
                        'range'      => ['p'],
                        'expected'   => '<p>parislondon<p>'
                    ],
                '[12] string' => [
                        'testValue'  => 12345,
                        'type'       => 'string',
                        'default'    => null,
                        'range'      => '',
                        'expected'   => null
                    ],
                '[13] string ok' => [
                        'testValue'  => 'Änderungen der Öffnungszeiten in Ürdingen. Paul\'s pages.',
                        'type'       => 'string',
                        'default'    => null,
                        'range'      => '',
                        'expected'   => 'Änderungen der Öffnungszeiten in Ürdingen. Paul\'s pages.'
                    ],
                '[14] float' => [
                        'testValue'  => 3.14159265,
                        'type'       => 'float',
                        'default'    => 0.0,
                        'range'      => ['min' => 0.0, 'max' => 10.0, 'use' => 'near' ],
                        'expected'   => 3.14159265
                    ],
                '[15] decimal' => [
                        'testValue'  => "3,1415926535898",
                        'type'       => 'decimal',
                        'default'    => 0.0,
                        'range'      => ['min' => 0.5, 'max' => 10.5, 'use' => 'near' ],
                        'expected'   => 3.1415926535898
                    ],
                '[16] decimal 2' => [
                        'testValue'  => "1.123,98 €",
                        'type'       => 'decimal',
                        'default'    => 0.00,
                        'range'      => "",
                        'expected'   => 1123.98
                    ],
                '[17] datetime' => [
                        'testValue'  => "2023-02-11 15:43:00",
                        'type'       => 'datetime',
                        'default'    => "1972-01-01 01:00:00",
                        'range'      => "",
                        'expected'   => "2023-02-11 15:43:00"
                    ],
                '[18] datetime_local' => [
                        'testValue'  => "2023-02-11T15:43:00",
                        'type'       => 'datetime_local',
                        'default'    => "1972-01-01T01:00:00",
                        'range'      => "",
                        'expected'   => "2023-02-11T15:43:00"
                    ],
                '[19] number out of range' => [
                        'testValue'  => 17,
                        'type'       => 'number',
                        'default'    => null,
                        'range'      => ['min' => 0, 'max' => 10, 'use' => 'default'],
                        'expected'   => null
                    ],
                '[20] number out of range (min)' => [
                        'testValue'  => 17,
                        'type'       => 'number',
                        'default'    => null,
                        'range'      => ['min' => 1, 'max' => 10, 'use' => 'min'],
                        'expected'   => 1
                    ],
                '[21] number out of range (max)' => [
                        'testValue'  => 17,
                        'type'       => 'number',
                        'default'    => null,
                        'range'      => ['min' => 0, 'max' => 10, 'use' => 'max'],
                        'expected'   => 10
                    ],
                '[22] number out of range (near)' => [
                        'testValue'  => 17,
                        'type'       => 'number',
                        'default'    => null,
                        'range'      => ['min' => 1, 'max' => 8, 'use' => 'near'],
                        'expected'   => 8
                    ],
                '[23] number out of range (near low)' => [
                        'testValue'  => -17,
                        'type'       => 'number',
                        'default'    => null,
                        'range'      => ['min' => -10, 'max' => 10, 'use' => 'near'],
                        'expected'   => -10
                    ],
                '[24] date (null)' => [
                        'testValue'  => null,
                        'type'       => 'date',
                        'default'    => null,
                        'range'      => '',
                        'expected'   => null
                    ],
                '[25] date range out (near) min' => [
                        'testValue'  => "1848-03-23",
                        'type'       => 'date',
                        'default'    => '1980-01-01',
                        'range'      => ['min' => '1940-01-01', 'max' => '2038-12-31', 'use' => 'near'],
                        'expected'   => '1940-01-01'
                    ],
                '[26] date range out (near) max' => [
                        'testValue'  => "2233-03-23",
                        'type'       => 'date',
                        'default'    => null,
                        'range'      => ['min' => '1998-01-01', 'max' => '2038-12-31', 'use' => 'near'],
                        'expected'   => '2038-12-31'
                    ],
                '[27] datetime range out (near) min' => [
                        'testValue'  => "2024-08-10 08:30:00", // 'Y-m-d H:i:s
                        'type'       => 'datetime',
                        'default'    => null,
                        'range'      => ['min' => '2024-08-10 09:00:00', 'max' => '2024-08-10 12:30:00', 'use' => 'near'],
                        'expected'   => '2024-08-10 09:00:00'
                    ],
                '[28] time range out (near) max' => [
                        'testValue'  => "04:00", // 'Y-m-d H:i:s
                        'type'       => 'time',
                        'default'    => null,
                        'range'      => ['min' => '09:00', 'use' => 'near'],
                        'expected'   => '09:00'
                    ]
        ];
    }
    
    #[DataProvider('buildSGetStringToArrayDataset')]
    public function testGetStringToArray(
        string $testValue,
        string|null $delimiter,
        string|null $subdelimiter,
        string|int|array|float|null $expected
    ) {
        if ((is_null($delimiter)) && (is_null($subdelimiter)))
        {
            $actual = LEPTON_request::getStringToArray($testValue);
        }
        elseif (is_null($subdelimiter))
        {
            $actual = LEPTON_request::getStringToArray($testValue, $delimiter);
        }
        elseif ((is_null($delimiter)) && (!is_null($subdelimiter)))
        {
            $actual = LEPTON_request::getStringToArray($testValue, subdelimiter:$subdelimiter);
        }
        else
        {
            $actual = LEPTON_request::getStringToArray($testValue, $delimiter, $subdelimiter);
        }
        $this->assertEquals($expected, $actual);
    }
    
    static function buildSGetStringToArrayDataset(): array
    {
        return [
            'simple string' => [
                'testValue' => "job:stay;value:123",
                'delimiter' => null,
                'subdelimiter' => null,
                'expected' =>  ['job' => 'stay', 'value' => '123']
            ],
            'other delimiters' => [
                'testValue' => "job = stay ?value= 123",
                'delimiter' => "?",
                'subdelimiter' => "=",
                'expected' =>  ['job' => 'stay', 'value' => '123']
            ],
            'only subdelimiters' => [
                'testValue' => "job=stay;value=123",
                'delimiter' => null,
                'subdelimiter' => "=",
                'expected' =>  ['job' => 'stay', 'value' => '123']
            ],
            'witch empty' => [
                'testValue' => "job=stay;value=123;;zusatz=12",
                'delimiter' => null,
                'subdelimiter' => "=",
                'expected' =>  ['job' => 'stay', 'value' => '123', 'zusatz' => '12']
            ]
        ];
    
    }
}

<?php

/**
 *  [0] Basics (configurations and settings)
 *  php.ini:
 *      pdo_mysql.default_socket = /Applications/MAMP/tmp/mysql/mysql.sock
 *
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LeptonLeptokenTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonLeptokenTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonLeptokenTest.php
 *
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\TestCase;

class LeptonLeptokenTest extends TestCase
{
    public function setUp(): void
    {

        //  [1]
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        require_once dirname(__DIR__) . "/upload/config/config.php";

        $templateOutput = "";
        $oLEPTON = \LEPTON_frontend::getInstance();

        \LEPTON_core::getInstance()->getProtectedFunctions($templateOutput, $oLEPTON);
    }

    /**
     * [1] Simple first run: get the content of the $_SESSION
     */
    public function testGetSystemLanguage()
    {
        echo \LEPTON_tools::display($_SESSION['LepTokens'] ?? 'LepTokens not found in "$_Session"!');

        // actual to get a (in-) vaild test
        $actual = \lib_r_filemanager::getSystemLanguage();
        $this->assertEquals("en_EN", $actual);
    }

    /**
     * [2] Second: get the content of the $_SESSION
     */
    public function testNumberTwo()
    {
        echo \LEPTON_tools::display($_SESSION['LepTokens'] ?? 'LepTokens not found in "$_Session"!');

        // actual to get a (in-) vaild test
        $actual = \lib_r_filemanager::getSystemLanguage();
        $this->assertEquals("en_EN", $actual);
    }

    public function testCheckLepToken()
    {
        $_POST['leptoken'] = $_SESSION['LepTokens'][0];
        // checkLepToken
        $oCLASS = new \LEPTON_securecms();
        $actual = $oCLASS->checkLepToken();
        $this->assertEquals(true, $actual);
    }

    /**
     * [2] Second: get the content of the $_SESSION
     */
    public function testNumberFour()
    {
        echo \LEPTON_tools::display($_SESSION['LepTokens'] ?? 'LepTokens not found in "$_Session"!');

        // actual to get a (in-) vaild test
        $actual = \lib_r_filemanager::getSystemLanguage();
        $this->assertEquals("en_EN", $actual);
    }
}

<?php

/**
 *  The LEPTON database connector.
 *
 */
interface LEPTON_database_interface
{
    /**
     *  Return the last error.
     *
     * @return string
     */
    public function get_error(): string;

    /**
     *  Check if there occurred any error
     *
     * @return boolean
     */
    public function is_error(): bool;

    /**
     *  Get the MySQL DB handle
     *
     * @return resource or boolean false if no connection is established
     */
    public function get_db_handle(): object;

    /**
     *  Get the internal DB key
     *
     * @return string
     */
    public function get_db_key(): string;

    /**
     *  Establish the connection to the desired database defined in /config.php.
     *
     *  This function does not connect multiple times, if the connection is
     *  already established the existing database handle will be used.
     *
     * @param array $settings Assoc. array within optional settings. Pass by reference!
     * @return void
     *
     * @notice Param 'settings' is an assoc. array with the connection-settings, e.g.:
     *            $settings = array(
     *                'host'    => "example.tld",
     *                'user'    => "example_user_string",
     *                'pass'    => "example_user_password",
     *                'name'    => "example_database_name",
     *                'port'    => "1003",
     *                'key'     => "a unique key",      // optional
     *              'cipher'    => "aes-256-cbc",       // optional
     *              'iv'        => "12_%#0123773345_",  // MUST be match to the given 'cipher'!
     *              'ivlen'     => 16,                  // MUST be set if 'iv' has a different length than 16!
     *                'charset' => "utf8"
     *            );
     *
     *          KEEP in mind, that the optional keys are set in the lepton.ini.php!
     *          So if you try to use the secure_* methods on a new connection via the $settings there
     *          can be causes some unintended results if these keys are missing or formatted!
     *
     *            To set up the connection to another charset as 'utf8' you can
     *            also define another one inside the config.php e.g.
     *                define('DB_CHARSET', 'utf8');
     *
     */
    function connect(array &$settings = []): void;

    /**
     *  Execute a mysql-query without returning a result.
     *  A typical use is e.g. "DROP TABLE IF EXIST" or "SET NAMES ..."
     *
     * @param string $sMySQL_Query Any (MySQL-) Query
     * @param array $aParams Optional array within the values if place-holders are used.
     *
     * @return bool    True if success, otherwise false.
     *
     * @code{.php}
     *      LEPTON_database::getInstance()->simple_query("DROP TABLE `example` IF EXIST");
     *      // or
     *      LEPTON_database::getInstance()->simple_query(
     *          "INSERT into `TABLE_example` (`name`,`state`) VALUES('?','?');",
     *              array(
     *                  ['example','none'],
     *                  ['master', 'confirmed']
     *              )
     *      );
     *      // or
     *      LEPTON_database::getInstance()->simple_query(
     *          "INSERT into `TABLE_example` (`name`,`state`) VALUES( :name , :sta );",
     *              array(
     *                  "name"  => "Aldus",
     *                  "sta"   => "internal"
     *              )
     *      );
     * @endcode
     */
    public function simple_query(string $sMySQL_Query = "", array $aParams = array()): bool;

    /**
     *    Execute a SQL query and return the first row of the result array
     *
     * @param string $SQL Any SQL-Query or statement
     *
     * @return   mixed   Value of the table-field or NULL for error
     *
     * @notice Changed in L* 4.5.0! There may be side-effects!
     */
    public function get_one(string $SQL): mixed;

    /**
     *    Returns a linear array within the table-names of the current database
     *
     * @param string $strip A string to 'strip' chars from the table-names, e.g. the prefix.
     * @return array|bool An array within the table-names of the current database, or FALSE (bool).
     *
     */
    public function list_tables(string $strip = ""): array|bool;

    /**
     *  Placed for all fields from a given table(-name) an assoc. array
     *  inside a given storage-array.
     *
     * @param string $sTableName A valid table-name.
     * @param array $aStorage An array to store the results. Call by reference!
     *
     * @return bool    True if success, otherwise false.
     *
     */
    public function describe_table(string $sTableName, array &$aStorage = array(), int $iForm = self::DESCRIBE_RAW): bool;

    /**
     *  Public "shortcut" for executing a single mySql-query without passing values.
     *
     *
     * @param string $aQuery A valid mySQL query.
     * @param bool $bFetch Fetching the result - default is false.
     * @param array $aStorage A storage array for the fetched results. Pass by reference!
     * @param bool $bFetchAll Try to get all entries. Default is true.
     * @return   bool    False if fails, otherwise true.
     *
     * @example
     *      $results_array = array();
     *      $this->db_handle->execute_query(
     *          "SELECT * from ".TABLE_PREFIX."pages WHERE page_id = '".$page_id."' ",
     *          true,
     *          $results_array,
     *          false
     *      );
     *
     *
     */
    public function execute_query(string $aQuery = "", bool $bFetch = false, array &$aStorage = array(), bool $bFetchAll = true): bool;

    /**
     *  Public function to build and execute a mySQL query direct.
     *  Use this function/method for update and insert values only.
     *  As for a simple select you can use "prepare_and_execute" above.
     *
     * @param string $type A "job"-type: this time only "update" and "insert" are supported.
     * @param string $table_name A valid table-name (incl. table-prefix).
     * @param array $table_values An array within the table-field-names and values.
     * @param string $condition An optional condition for "update" - this time a simple string.
     * @param string $key An optional "no update" key field to be excluded on update while "insert_on_duplicate_key_update" - a simple string containing 1 key field.
     *
     * @return bool    False if fails, otherwise true.
     *
     */
    public function build_and_execute(string $type, string $table_name, array $table_values, string $condition = "", string $key = ""): bool;

    /**
     *  Execute a SQL query and return a handle to queryMySQL
     *
     * @param string $sSqlQuery The query string to be executed.
     * @return object|null   Object or NULL for error
     *
     */
    public function query(string $sSqlQuery = ""): object|null;

    /**
     *  Add column to existing table
     *
     * @param string $table The table name to be extended.
     * @param string $column Name
     * @param string $desc column type and default
     *
     * @return bool   true.
     * @code{.php}
     *      LEPTON_database::getInstance()->add_column('news','new_name','varchar(64) NOT NULL DEFAULT "0"');
     * @endcode
     */
    public function add_column(string $table = '', string $column = '', string $desc = ''): bool;
}

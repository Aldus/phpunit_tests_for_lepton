<?php

/**
 *  [0] Basics
 *
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LeptonHandleTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonHandleTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonHandleTest.php
 *
 */

declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use LEPTON_handle;
use LEPTON_tools;

//  [4] Here we go
class LeptonHandleTest extends TestCase
{
    public function setUp(): void
    {
        //  [1.1]
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [1.2]
        require_once dirname(__DIR__) . "/upload/framework/classes/lepton_handle.php";

        //  [1.3]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }
    }

    #[DataProvider('buildCheckPasswordChars')]
    public function testCheckPasswordChars(string $value, bool $expected)
    {
        $actual = LEPTON_handle::checkPasswordChars($value);
        $this->assertEquals($expected, $actual);
    }

    /**
     *  This is the data-provider for the test above 'testCheckPasswordChars'.
     */
    public static function buildCheckPasswordChars(): array
    {
        return [
            'simple password' => ['value' => 'abcdefghik',  'expected' => true],
            'special chars'   => ['value' => '@?koberger',  'expected' => true],
            'special failed'  => ['value' => '\~?koberger', 'expected' => false]
        ];
    }

    #[DataProvider('buildArraysToSort')]
    public function testArraOrderby(array $testValues, string $sortBy, array $expected)
    {
        $actual = LEPTON_handle::array_orderby($testValues, $sortBy);
        $this->assertEquals($expected, $actual);
    }

    /**
     *  This is the data-provider for the test above 'testArraOrderby'.
     */
    public static function buildArraysToSort(): array
    {
        return [
            'names' => [
                'testValues' => [
                    ['name' => 'Wittgenstein',      'vorname' => 'Ludwig',  'id' => 123],
                    ['name' => 'Beethoven, van',    'vorname' => 'Ludwig',  'id' =>  23],
                    ['name' => 'Zwaart',            'vorname' => 'Piet',    'id' =>  13],
                    ['name' => 'Mies van der Rohe', 'vorname' => 'Ludwig',  'id' => 201],
                    ['name' => 'manutius, d. Ä.',   'vorname' => 'aldus',   'id' =>  11],
                    ['name' => 'Zwaart',            'vorname' => 'Piet',    'id' =>  10]
                ],
                'sortBy'    => 'vorname, name, id',
                'expected' => [
                    ['name' => 'manutius, d. Ä.',   'vorname' => 'aldus',   'id' =>  11],
                    ['name' => 'Beethoven, van',    'vorname' => 'Ludwig',  'id' =>  23],
                    ['name' => 'Mies van der Rohe', 'vorname' => 'Ludwig',  'id' => 201],
                    ['name' => 'Wittgenstein',      'vorname' => 'Ludwig',  'id' => 123],
                    ['name' => 'Zwaart',            'vorname' => 'Piet',    'id' =>  10],
                    ['name' => 'Zwaart',            'vorname' => 'Piet',    'id' =>  13]
                ]
            ]
        ];
    }
}

<?php

/**
 *  [0] Basics
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/FunctionGetLeptokenTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionGetLeptokenTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionGetLeptokenTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionGetLeptokenTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

//  [4] Here we go
class FunctionGetLeptokenTest extends TestCase
{
    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2]
        if (!defined("LANGUAGE")) {
            define('LANGUAGE', "EN");
        }

        //  [3]
        //require_once dirname(__DIR__) . "/upload/framework/classes/lepton_core.php";

        //  [4]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }
        $GLOBALS['_POST']['leptoken'] = 'a0000000001111111111222222222243';
    }

    #[DataProvider('getLeptokenProvider')]
    public function testGetLeptoken(string $where, string $leptoken, string $expected): void
    {

        \LEPTON_handle::register("get_leptoken");
        echo \LEPTON_tools::display($_GET);
        $actual = get_leptoken();
        $this->assertEquals($expected, $actual);
    }
    
    public static function getLeptokenProvider(): array
    {
        return [
            'simple POST' => [
                    'where'  => 'post',
                    'leptoken'  => 'a0000000001111111111222222222243',
                    'expected'  => 'a0000000001111111111222222222243'
                ],
            'simple GET' => [
                    'where'  => 'get',
                    'leptoken'  => 'a0000000001111111111222222222243',
                    'expected'  => 'a0000000001111111111222222222243'
                ],
            'simple GET entity' => [
                    'where'  => 'get entity',
                    'leptoken'  => 'a0000000001111111111222222222243',
                    'expected'  => 'a0000000001111111111222222222243'
                ]
        ];
    }
}

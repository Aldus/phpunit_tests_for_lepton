<?php

/**
 *  [0] Basics
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/FunctionCreateICalTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionCreateICalTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionCreateICalTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/FunctionCreateICalTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use LEPTON_handle;

//  [4] Keep in mind MAMP (-server) is running
require dirname(__DIR__)."/upload/config/config.php";

//  [5] Here we go
class FunctionCreateICalTest extends TestCase
{
    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);
        
        //  [2]
        LEPTON_handle::register("create_iCal_file");

    }

    #[DataProvider('calDataProvider')]
    public function testCreate_iCal_file(array $data, int $expected): void
    {
        
        $actual = create_iCal_file(
            $data['title'],
            $data['location'],
            // --
            $data['startdate'],
            $data['starttime'],
            // --
            $data['enddate'],
            $data['endtime'],
            // --
            $data['description'],
            // --
            $data['reminder'],
            $data['reminder_text'],
            // --
            $data['filename']
        );
        
        $this->assertEquals($expected, $actual);
    }
    
    public static function calDataProvider(): array
    {
        return [
            'simple all data' => [
                'data'  => [
                    "title"          => "Aldus Testdatum",
                    "location"       => 'Bielefeld',
                    "startdate"      => '2025-03-11',
                    "starttime"      => '10:30',
                    "enddate"        => '2025-03-11',
                    "endtime"        => '12:00',
	                "description"    => 'Das ist nur ein Testdatum. (Aldus)',
	                "reminder"       => '60',
	                "reminder_text"  => 'Erinnerungstext',
	                "filename"       => dirname(__DIR__) . "/upload/temp".'/test_event.ics'  
                ],
                'expected'  => 608
            ]
        ];
    }
}

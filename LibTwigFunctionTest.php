<?php

/**
 *  [0] Basics
 *
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LibTwigFunctionTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibTwigFunctionTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibTwigFunctionTest.php
 *
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LibTwigFunctionTest.php
 *
 */

// IMPORTANT: we are also testing TWiG-filters here!

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]


use lib_twig_box;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

//  [4] Here we go
class LibTwigFunctionTest extends TestCase
{
    public ?lib_twig_box $oTWIG = null;

    public function setUp(): void
    {

        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2]
        require_once dirname(__DIR__) . "/upload/config/config.php";

        $this->oTWIG = lib_twig_box::getInstance();
        $this->oTWIG->registerPath(__DIR__ . "/templates", 'phpunit');
    }

    #[DataProvider('getDisplayPattern')]
    public function testGetDisplayPattern(string $value, string $expected)
    {
        $data = [
            'name' => 'testinput',
            'placeholder' => 'please type your name',
            'value' => $value
        ];
        $actual = $this->oTWIG->render(
            '@phpunit/generateUsernameInput.lte',
            $data
        );
        $this->assertEquals($expected, $actual);
    }

    public static function getDisplayPattern(): array
    {
        return [
            'first' => [
                'value' => "",
                'expected'  => 'pattern="[a-zA-Z0-9@ _\-,.äöüÄÖÜ:]{3,128}"' . "\n"
            ]
        ];
    }

    // 2
    #[DataProvider('getPasswordPattern')]
    public function testGetPasswordPattern(string $value, string $expected)
    {
        $data = [
            'name' => 'testinput',
            'placeholder' => 'please type your name',
            'value' => $value
        ];
        $actual = $this->oTWIG->render(
            '@phpunit/generatePasswordPattern.lte',
            $data
        );
        $this->assertEquals($expected, $actual);
    }

    public static function getPasswordPattern(): array
    {
        return [
            'first' => [
                'value' => "",
                'expected'  => 'pattern="[a-zA-Z0-9_\-!$ยง#*+@?µäöüÄÖÜœæŒÆ µ]{6,128}"'
            ]
        ];
    }
    
    #[DataProvider('getTestTimes')]
    public function testTimeF(string $testTimeStr, string $expected)
    {
        $data = [
           'testTimeStr' => $testTimeStr 
        ];
        
        $actual = $this->oTWIG->render(
            '@phpunit/timeF_test.lte',
            $data
        );
        
        $this->assertEquals($expected, $actual);
    
    }
    
    public static function getTestTimes(): array
    {
        return [
            'simple' => [
                'testTimeStr' => '12:40:23 any z',
                'expected'    => '12:40'
            ],
            '0 (string)' => [
                'testTimeStr' => '0',
                'expected'    => '00:00'
            ],
            'empty string' => [
                'testTimeStr' => '',
                'expected'    => ''
            ],
            'wrong format' => [
                'testTimeStr' => 'This is no :time(-string)!',
                'expected'    => 'This is no :time(-string)!'
            ]
        ];
    }

    #[DataProvider('getOperatorTestValues')]
    public function testOperators(bool $valueA, bool $valueB, string $expected)
    {
        $data = [
           'valueA' => $valueA,
           'valueB' => $valueB
        ];
        
        $actual = $this->oTWIG->render(
            '@phpunit/operators.lte',
            $data
        );
        
        $this->assertEquals($expected, $actual);
    }
    /**
     * 
     * char 1 == test with "&&" [and]   on v1(true) and v2 (true)
     * char 2 == test with "||" [or ]   on v1(true) and v2 (true)
     * char 3 == test with "?:" [elvis] on v1(true) and v2 (true)
     * 
     * @return array
     */
    public static function getOperatorTestValues(): array
    {
        return [
            'both true' => [
                'valueA' => true,
                'valueB' => true,
                'expected' => "111"
            ],
            'one true' => [
                'valueA' => true,
                'valueB' => false,
                'expected' => "011"
            ],
            'one true (2)' => [
                'valueA' => false,
                'valueB' => true,
                'expected' => "011"
            ],
            'both false' => [
                'valueA' => false,
                'valueB' => false,
                'expected' => "000"
            ]
        ];
    }
}

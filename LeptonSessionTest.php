<?php

/**
 *  [0] Basics
 *
 *  @category PHPUnit
 *  @package  LEPTON-CMS
 *  @author   Dietrich Roland Pehlke <drp@cms-lab.com>
 *  @license  cc 3.0 sa-by
 *  @version  PHP 8.3.1
 *  @link     https://gitlab.com/Aldus/phpunit_tests_for_lepton GiT
 *  @link     https://gitlab.com/lepton-cms/LEPTON
 *
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' tests/LeptonSessionTest.php
 *   php phpunit.phar --colors='always' --display-warnings tests/LeptonSessionTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonSessionTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonSessionTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonSessionTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\TestCase;

//  [4] Here we go
class LeptonSessionTest extends TestCase
{
    public function setUp(): void
    {
        $leptonBasePath = dirname(__DIR__) . "/upload/framework/";

        //  [1.1] These "requirements" could be in an external file declared for L*
        require_once $leptonBasePath . "functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [1.2]
        require_once $leptonBasePath . "classes/lepton_session.php";

        //  [1.3]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }

        //  [1.4]
        if (!defined("LANGUAGE")) {
            define('LANGUAGE', "EN");
        }
    }

    public function testGetCookieDefaults()
    {
        $actual = \LEPTON_session::get_cookie_defaults();

        echo \LEPTON_tools::display($actual);

        $expected = [
            'expires'   => time() + (4 * 3600),
            'path'      => '/',
            'domain'    => '',
            'secure'    => false,
            'httponly'  => true,
            'samesite'  => 'Lax'
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testGetConfigValuesFromIni()
    {
        $actual = \LEPTON_session::getConfigValuesFromIni();

        echo \LEPTON_tools::display($actual);

        $expected = [
            'samesite' => 'Lax',
            'httponly' => true,
            'path' => '/',
            'lifetime' => 14400
        ];

        $this->assertEquals($expected, $actual);
    }
}

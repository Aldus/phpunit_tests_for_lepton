<?php

/**
 *  @example
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/RegexpSubdomainTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/RegexpSubdomainTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/RegexpSubdomainTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/RegexpSubdomainTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class RegexpSubdomainTest extends TestCase
{
    /**
     *  Mindestens eine subdomain:
     *  aladin@name.tld     => 0 := false
     *  aladin@sub.name.tld => 1 := true
     */
    public const TEST_PATTERN = "/[\w\.]+[@][\w]+[\.][\w]+[\.][\w]+/U";

    public function setUp(): void
    {

    }

    #[DataProvider('buildRegExpresion')]
    public function testRegExpresion(string $value, int $expected)
    {
        $actual = preg_match(self::TEST_PATTERN, $value);
        $this->assertEquals($expected, $actual);
    }

    public static function buildRegExpresion(): array
    {
        return [
            'Email einfach' => [
                    'value' => "test@none.tld",
                    'expected'  => 0
                ],
            'Email subdomain'   => [
                    'value' => "test@subdomain.tld.de",
                    'expected'  => 1
                ],
            'Email mehr subdomains' => [
                    'value' => "test.name@sub1.sub2.sub3.tld",
                    'expected'  => 1
                ],
            'Email einfach + sonderzeichen' => [
                    'value' => "test.name-www.abd@domain.tld",
                    'expected'  => 0
                ],
            'Email zwei subdomains + sonderzeichen' => [
                    'value' => "<vorname.name>test.name@sub2.sub3.tld",
                    'expected'  => 1
            ]
        ];
    }
}

<?php

/**
 *  [0] Basics
 *      PHPUnit 11.4.3 
 *
 *  to get phpunit use
 *
 *  wget -O phpunit https://phar.phpunit.de/phpunit-11.phar
 *
 *  @example
 *
 *   cd /Applications/MAMP/htdocs/projekte/LEPTON_VII
 *   php phpunit.phar --colors='always' --display-warnings tests/LeptonCoreTest.php
 *
 *   phpcs --colors --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonCoreTest.php
 *   phpcbf --standard=PSR12 /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonCoreTest.php
 *
 *   php phpstan.phar analyse  /Applications/MAMP/htdocs/projekte/LEPTON_VII/tests/LeptonCoreTest.php
 */

//  [1]
declare(strict_types=1);

//  [2]
namespace Lepton\tests;

//  [3]
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use LEPTON_core;

//  [4] Here we go
class LeptonCoreTest extends TestCase
{
    public function setUp(): void
    {
        //  [1] These "requirements" could be in an external file declared for L*
        require_once dirname(__DIR__) . "/upload/framework/functions/function.lepton_autoloader.php";
        spl_autoload_register("lepton_autoloader", true);

        //  [2.0]
        require_once dirname(__DIR__) . "/upload/config/config.php";

        //  [2.1]
        if (!defined("LANGUAGE")) {
            define('LANGUAGE', "EN");
        }

        //  [3]
        require_once dirname(__DIR__) . "/upload/framework/classes/lepton_core.php";

        //  [4]
        if (!defined("LEPTON_PATH")) {
            define('LEPTON_PATH', dirname(__DIR__) . "/upload/");
        }
    }

    public function fillSuperglobal(string $where, string $name, string|int|array $value): void
    {
        switch ($where) {
            case 'post':
                $_POST[$name] = $value;
                break;

            case 'get':
                $_GET[$name] = $value;
                break;

            case 'session':
                $_SESSION[strtoupper($name)] = $value;
                break;

            case 'server':
                $_SERVER[$name] = $value;
                break;
        }
    }

    #[DataProvider('buildGetValueData')]
    public function testGetValue(string $name, string $type, string|int $value, bool|string|int|null $expected): void
    {
        $_POST[$name] = $value;

        $actual = LEPTON_core::getValue($name, $type);
        unset($_POST[$name]);
        $this->assertEquals($expected, $actual);
    }

    /**
     *  This is the data-provider for the test above 'testGetValue'.
     *
     * @return array<mixed>
     */
    public static function buildGetValueData(): array
    {
        return [
            'string is ok' => [
                    'name' => 'aldus_test11',
                    'type'  => 'string_clean',
                    'value' => 'abcdefghik',
                    'expected' => "abcdefghik"
                ],
            'string failed' => [
                    'name' => 'aldus_test21',
                    'type'  => 'string_clean',
                    'value' => 567,
                    'expected' => null
                ],
            'int as str ok' => [
                    'name' => 'aldus_test31',
                    'type'  => 'string_clean',
                    'value' => '12345',
                    'expected' => '12345'
                ]
        ];
    }

    #[DataProvider('buildGetValueData2')]
    public function testGetValue2(
        string $where,
        string $name,
        string $type,
        string|int|float|array|null $value,
        bool|string|int|array|null $expected,
        string|null $delimiter='X',
        string|int|float|array|null $default=null,
        string|int|float|array|null $range=null
    ): void {
        $this->fillSuperglobal($where, $name, $value);
        $actual = LEPTON_core::getValue($name, $type, $where, $delimiter, $default, $range);
        $this->assertEquals($expected, $actual);
    }

    /**
     *  This is the data-provider for the test above 'testGetValue2'.
     *
     * @return array<mixed>
     */
    public static function buildGetValueData2(): array
    {
        return [
            'string is ok2' => [
                    'where'    => 'post',
                    'name'     => 'aldus_test113',
                    'type'     => 'string_clean',
                    'value'    => 'Geh aus mein Herz und suche Freud\'.',
                    'expected' => 'Geh aus mein Herz und suche Freud&#039;.',
                    'delimiter' => "X",
                    'default'   => null
                ],
            'string failed2'    => [
                    'where'     => 'session',
                    'name'      => 'aldus_test213',
                    'type'      => 'string_clean',
                    'value'     => 567,
                    'expected'  => null,
                    'delimiter' => "X",
                    'default'   => null
                ],
            'int as str sollte ok sein (get)' => [
                    'where'     => 'post',
                    'name'      => 'aldus_test313',
                    'type'      => 'string_clean',
                    'value'     => 'AB3.14159265',
                    'expected'  => 'AB3.14159265',
                    'delimiter' => "X",
                    'default'   => null
                ],
            'string_secure' => [
                    'where'     => 'session',
                    'name'      => 'C<',
                    'type'      => 'string_secure',
                    'value'     => 'string_secure-',
                    'expected'  => 'string_secure-',
                    'delimiter' => "X",
                    'default'   => null
                ],
            'string_secure faild' => [
                    'where'     => 'post',
                    'name'      => 'string_securetest2',
                    'type'      => 'string_secure',
                    'value'     => 'string_secure-*',
                    'expected'  => null,
                    'delimiter' => "X",
                    'default'   => null
                ],
            'integers in array' => [
                    'where'     => 'post',
                    'name'      => 'integer_in_array',
                    'type'      => 'integer',
                    'value'     => [1, 22, 34, "123", "Aladin ist keine Zahl"],
                    'expected'  => [1, 22, 34, 123, null],
                    'delimiter' => "X",
                    'default'   => null
                ],
            'strings in array' => [
                    'where'     => 'post',
                    'name'      => 'strings_in_array',
                    'type'      => 'string_clean',
                    'value'     => ["123", "Aladin ist ein Text", 123, [1,2,3,4,5,6]],
                    'expected'  => ["123", "Aladin ist ein Text", null, null],
                    'delimiter' => "X",
                    'default'   => null
                ],
            'using delimiter' => [
                    'where'     => 'session',
                    'name'      => 'USING_DELIMITERS',
                    'type'      => 'string_clean',
                    'value'     => "Geht;aus;Test",
                    'expected'  => ["Geht", "aus", "Test"],
                    'delimiter' => ";",
                    'default'   => null
                ],
            'using delimiter in array' => [
                    'where'     => 'session',
                    'name'      => 'USING_DELIMITERS',
                    'type'      => 'string_clean',
                    'value'     => ["Geht;aus;Test", "1;2;3", "a;b;c;d"],
                    'expected'  => [["Geht", "aus", "Test"],["1", "2", "3"], ["a", "b", "c", "d"]],
                    'delimiter' => ";",
                    'default'   => null
                ],
            'using default' => [
                    'where'     => 'post',
                    'name'      => 'using defaults',
                    'type'      => 'decimal',
                    'value'     => "",
                    'expected'  => "1.00",
                    'delimiter' => "X",
                    'default'   => "1.00"
                ],
            'using range (cut)' => [
                    'where'     => 'post',
                    'name'      => 'using_range',
                    'type'      => 'string',
                    'value'     => "abcdefghijk",
                    'expected'  => "abcd",
                    'delimiter' => "X",
                    'default'   => "-",
                    'range'     => ['min' => 2, 'max' => 4, 'use' => 'cut']
                ],
                'using range (int)' => [
                    'where'     => 'post',
                    'name'      => 'using_range',
                    'type'      => 'integer',
                    'value'     => 123,
                    'expected'  => 100,
                    'delimiter' => "X",
                    'default'   => 0,
                    'range'     => ['min' => 2, 'max' => 100, 'use' => 'near']
                ],
                'using range (chars)' => [
                    'where'     => 'post',
                    'name'      => 'using_range',
                    'type'      => 'string_allowed',
                    'value'     => "abc<b>wer<b><p>a</p> <em>123</em>",
                    'expected'  => "abc<b>wer<b>a <em>123</em>",
                    'delimiter' => "X",
                    'default'   => "-",
                    'range'     => ['b','em']
                ]
        ];
    }

    // L*VII
    public function testimageTypesAllowed()
    {
        $actual = LEPTON_core::imageTypesAllowed();
        $expected = [ 0 => 'gif', 'jpg', 'jpeg', 'png', 'svg', 'webp', 'avif', 'jxl'];
        $this->assertEquals($expected, $actual);
    }
}
